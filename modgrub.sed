#!/bin/sed -f

/^submenu --hotkey=a 'Advanced options [.][.][.]'/ {
	s/--hotkey=a/--hotkey=a --id=advanced/
}

/^\s\+menuentry '... Automated install'/ {
	s/menuentry/menuentry --id=auto/
}

/^play/ {
	i\
set default=advanced>auto
	i\
set timeout=0
}

