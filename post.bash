#!/bin/bash

set -e -o pipefail

apt --no-install-recommends -y install	\
	curl				\
	dbus				\
	git				\
	neovim				\
	rsync				\
	ssh				\
	podman				\
	ca-certificates			\
	crun				\
	rootlesskit			\
	dbus-user-session		\
	slirp4netns

group="$(id -gn 1000)"
user="$(id -nu 1000)"

home="/home/${user}"

mkdir -p "${home}/.ssh"
touch "${home}/.ssh/authorized_keys"
chown "${user}:${group}" "${home}/.ssh" "${home}/.ssh/authorized_keys"
chmod 0700 "${home}/.ssh"
chmod 0600 "${home}/.ssh/authorized_keys"

printf "%s\tALL=(ALL) NOPASSWD: ALL\n" "${user}" >"/etc/sudoers.d/${user}"

sed -i 's/#\?\(PermitRootLogin\)\s\+.*/\1 no/' '/etc/ssh/sshd_config'

eject

