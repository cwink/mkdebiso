#!/bin/ash

set -e -o pipefail

isourl='http://deb.debian.org/debian/dists/bookworm/main/installer-amd64/current/images/netboot/mini.iso'
isoreqsum='74e37f21f726fa13e9e6c1da900ed7e7  cache/mini.iso'

mkdir -p 'cache'

[ ! -f 'cache/mini.iso' ] && curl -o 'cache/mini.iso' "${isourl}"

if [ -f 'cache/mini.iso' ]; then
	isogensum="$(md5sum 'cache/mini.iso')"
	if [ "${isogensum}" != "${isoreqsum}" ]; then
		printf 'invalid iso checksum %s' "${isogensum}" >&2
		exit 1
	fi
fi

mkdir -p '/tmp/mini'

bsdtar -C '/tmp/mini' -f 'cache/mini.iso' -xv

chmod 0600 '/tmp/mini/initrd.gz'
gunzip '/tmp/mini/initrd.gz'
cpio -A -F '/tmp/mini/initrd' -H 'newc' -o <<-EOF
	post.bash
	preseed.cfg
EOF
gzip '/tmp/mini/initrd'
chmod 0400 '/tmp/mini/initrd.gz'

chmod 0600 '/tmp/mini/isolinux.cfg'
./modisolin.sed -i '/tmp/mini/isolinux.cfg'
chmod 0400 '/tmp/mini/isolinux.cfg'

chmod 0700 '/tmp/mini/boot/grub'
chmod 0600 '/tmp/mini/boot/grub/grub.cfg'
./modgrub.sed -i '/tmp/mini/boot/grub/grub.cfg'
chmod 0400 '/tmp/mini/boot/grub/grub.cfg'
chmod 0500 '/tmp/mini/boot/grub'

rm -f 'cache/repacked.iso'

xorriso	-as 'mkisofs'			\
	-rJ				\
	-b 'isolinux.bin'		\
	-c 'boot.cat'			\
	-no-emul-boot			\
	-boot-load-size 4		\
	-boot-info-table		\
	-eltorito-alt-boot		\
	--efi-boot 'boot/grub/efi.img'	\
	-no-emul-boot			\
	-o 'cache/repacked.iso'		\
	'/tmp/mini'

isohybrid -h 64 -s 32 -u 'cache/repacked.iso'

