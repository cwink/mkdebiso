#!/bin/sh

set -e

version='latest'
image="debian-builder/runtime:${version}"

podman build --no-cache --squash -t "${image}" '.'

podman run				\
	--rm				\
	--userns 'keep-id'		\
	-v "${PWD}:/mnt/pwd:Z"		\
	"${image}" './repack.ash'

